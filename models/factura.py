# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_factura(models.Model):

	_name = 'batoilogic.factura'

	fechaentrega = fields.Date('Fecha de entrega')
	albaran_id = fields.Many2one('batoilogic.albaran', 'Albaran')
	
