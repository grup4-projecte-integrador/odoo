# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_producto(models.Model):

	_name = 'batoilogic.producto'

	nombre = fields.Char(String='Nombre', size=40, required=True)
	descripcion = fields.Char(String='Descripcion', size=200)
	precio = fields.Float('Precio', (3,2))
	stock = fields.Integer(String='Stock', size=40)
	
