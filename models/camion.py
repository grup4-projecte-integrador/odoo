# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_camion(models.Model):

	_name = 'batoilogic.camion'

	matricula = fields.Char(String='Matricula', size=7, required=True)
	marca = fields.Char(String='Marca', size=40, required=True)
	modelo = fields.Char(String='Modelo', size=40, required=True)
	capacidad = fields.Integer(String='Capacidad', size=2, required=True)
