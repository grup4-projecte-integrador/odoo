
# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_ruta(models.Model):

	_name = 'batoilogic.ruta'

	repartidor_id = fields.Many2one('batoilogic.repartidor', 'Repartidor')
