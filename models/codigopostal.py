# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_postal(models.Model):

	_name = 'batoilogic.postal'

	numero = fields.Char(String='CP', size=40, required=True)
	ciudad_id = fields.Many2one('batoilogic.ciudad', 'Ciudad')
	
