# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_ciudad(models.Model):

	_name = 'batoilogic.ciudad'

	nombre = fields.Char(String='Nombre', size=40, required=True)
	provincia = fields.Selection([('A', 'Alicante'), ('V', 'Valencia'), ('CS', 'Castelló')])

