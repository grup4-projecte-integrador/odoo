# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_proveedor(models.Model):

	_name = 'batoilogic.proveedor'

	cif = fields.Char(String='CIF', size=40, required=True)
	direccion = fields.Char(String='Marca', size=40, required=True)
	telefono = fields.Char(String='Modelo', size=9, required=True)
	
