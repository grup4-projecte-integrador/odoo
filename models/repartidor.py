# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_repartidor(models.Model):

	_name = 'batoilogic.repartidor'

	nombre = fields.Char(String='Nombre', size=40, required=True)
	apellidos = fields.Char(String='Apellidos', size=40, required=True)
	dni = fields.Char(String='dni', size=9, required=True)
	password = fields.Char(String='password', size=9, required=True)
	camion_id = fields.Many2one('batoilogic.camion', 'Camion')

	_sql_constraints = [
           ('batoilogic_camion_uniq',
            'UNIQUE (camion_id)',
            'Un camion solo puede estar asignado a un repartidor')]
