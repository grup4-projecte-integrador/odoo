# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_pedidoprov(models.Model):

	_name = 'batoilogic.pedidoprov'

	descripcion = fields.Char("Descripcion", size=40, required=True)
	producto_id = fields.Many2one('batoilogic.producto', 'Pedido')
	proveedor_id = fields.Many2one('batoilogic.proveedor', 'Proveedor')

