# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_lineaspedido(models.Model):

	_name = 'batoilogic.lineaspedido'
	
	cantidad = fields.Integer(String='Cantidad', size=4, required=True)
	pedido_id = fields.Many2one('batoilogic.pedido', 'Pedido')
	producto_id = fields.Many2one('batoilogic.producto', 'Producto')

	_sql_constraints = [
        ('direccion_uniq',
         'UNIQUE (id, pedido_id)',
         'Esta linea ya existe.')]
