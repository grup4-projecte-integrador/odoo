# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_albaran(models.Model):

	_name = 'batoilogic.albaran'

	pedido_id = fields.Many2one('batoilogic.pedido', 'Pedido')
	ruta_id = fields.Many2one('batoilogic.ruta', 'Ruta')
