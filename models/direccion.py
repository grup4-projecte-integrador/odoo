# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_direccion(models.Model):

	_name = 'batoilogic.direccion'
	
	nombre = fields.Char(String='Nombre', size=40, required=True)
	tipo = fields.Selection([('calle', 'Calle'),('avenida', 'Avenida'), ('plaza', 'Plaza'), ('pasaje', 'Pasaje'), ('camino', 'Camino')], 'tipo')
	numero = fields.Integer(String='Numero', size=4, required=True)
	piso = fields.Integer(String='Piso', size=2)
	puerta = fields.Char(String='Puerta', size=1)
	cliente_id = fields.Many2one('batoilogic.cliente', 'Cliente')
	postal_id = fields.Many2one('batoilogic.postal', 'Postal')

	_sql_constraints = [
        ('direccion_uniq',
         'UNIQUE (id, cliente_id)',
         'Esta direccion ya esta enlazada a este cliente.')]
