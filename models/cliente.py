# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_cliente(models.Model):

	_name = 'batoilogic.cliente'

	nombre = fields.Char(String='Nombre', size=40, required=True)
	apellidos = fields.Char(String='Apellidos', size=40, required=True)
	fechanacimiento = fields.Date('Fecha de nacimiento')
	passwd = fields.Char(String='Contrasenya',required = True)
	user = fields.Char(String='Usuario',required = True)	
