# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_pertenecer(models.Model):

	_name = 'batoilogic.pertenecer'

	producto_id = fields.Many2one('batoilogic.producto', 'Producto')
	proveedor_id = fields.Many2one('batoilogic.proveedor', 'Proveedor')
	
	_sql_constraints = [
           ('batoilogic_pertenecer_uniq',
            'UNIQUE (producto_id, proveedor_id)',
            'Un producto solo puede pertenecer a un proveedor una vez')]

