# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_pedido(models.Model):

	_name = 'batoilogic.pedido'

	preciototal = fields.Float('Precio total', (3,2))
	observacion = fields.Char(String='Observacion', size=200, required=True)
	fechapedido = fields.Date(String='Fecha de pedido', size=40, required=True)
	fechaestimada = fields.Date(String='Fecha de entrega estimada', size=40, required=True)
	estado = fields.Selection([('EP', 'En preparacio'), ('P', 'Preparat'), ('ER', 'En ruta'), ('E', 'Entregat'), ('NE', 'No entregat')])
	cliente_id = fields.Many2one('batoilogic.cliente', 'Cliente')
	direccion_id = fields.Many2one('batoilogic.direccion', 'Direccion')
