# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoilogic_ubicacion(models.Model):

	_name = 'batoilogic.ubicacion'

	latitud = fields.Float(String='Latitud', size=40, required=True)
	longitud = fields.Float(String='Longitud', size=40, required=True)
	repartidor_id = fields.Many2one('batoilogic.repartidor', 'Repartidor')
	
